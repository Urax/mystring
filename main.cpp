#include "MyString.h"

int main()
{
    MyString s1;
    MyString s2;
    MyString sum;

    std::ofstream outf("output.txt");
    std::fstream inf("input.txt", std::ios::out);
    for(int i = 0; i < 10; ++i)
        inf << "Przykladowy napis " << i << endl;
    inf.close();
    inf.open("input.txt", std::ios::in);


    char z;
    while(1){
        cout << endl << "OPCJE:";
        cout << endl << "ci - zmien i-ty wyraz";
        cout << endl << "d - dodaj napisy";
        cout << endl << "p - porownaj napisy";
        cout << endl << "a - dodaj drugi napis do pierwszego napisu";
        cout << endl << "l - wypisz dlugosc napisow";
        cout << endl << "w - wyswietl napisy";
        cout << endl << "i - czytaj z pliku i zapisz do pierwszego napisu";
        cout << endl << "o - wpisz do pliku wartosc pierwszego napisu";
        cout << endl << "q - wyjsc" << endl;
        z = cin.get();
        switch(z)
        {
            case 'c':
                z = cin.get();
                if(z == '1')
                {
                    cin.ignore(INT_MAX, '\n');
                    cout << "Podaj pierwszy napis do zmiany (obecny napis: " << s1 << ")" << endl;
                    cin >> s1;
                }
                else if(z == '2')
                {
                    cin.ignore(INT_MAX, '\n');
                    cout << "Podaj drugi napis do zmiany (obecny napis: " << s2 << ")" << endl;
                    cin >> s2;
                }
                else
                    cin.ignore(INT_MAX, '\n');
                break;

            case 'a':
                s1 += s2;
                cout << "Wartosc napisu 1: " << s1 << endl;
                cin.ignore(INT_MAX, '\n');
                break;

            case 'i':
                inf >> s1;
                cin.ignore(INT_MAX, '\n');
                break;

            case 'o':
                outf << s1 << endl;
                cin.ignore(INT_MAX, '\n');
                break;

            case 'd':
                sum = s1 + s2;
                cout << "Dodane napisy: " << sum << endl;
                cin.ignore(INT_MAX, '\n');
                break;

            case 'w':
                cout << "Napis pierwszy: " << s1 << endl << "Napis drugi: " << s2 << endl;
                cin.ignore(INT_MAX, '\n');
                break;

            case 'p':
                if(s1 == s2)
                {
                    cout << "Napisy sa sobie rowne" << endl;
                }
                else if (s1 > s2)
                {
                    cout << "Napis pierwszy jest alfabetycznie pozniej od drugiego" << endl;
                }
                else
                {
                    cout << "Napis drugi jest alfabetycznie pozniej od pierwszego" << endl;
                }
                cin.ignore(INT_MAX, '\n');
                break;

            case 'l':
                cin.ignore(INT_MAX, '\n');
                cout << "Dlugosc pierwszego napisu " << s1.Length() << endl;
                cout << "Dlugosc drugiego napisu " << s2.Length() << endl;
                break;

            case 'q':
                cin.ignore(INT_MAX, '\n');
                return 0;

            default:
                cin.ignore(INT_MAX, '\n');
        }
    }

    outf.close();
    inf.close();
    return 0;

}
