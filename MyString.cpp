#include "MyString.h"

MyString::MyString(const char* value)
{
    this->~MyString();
    while(*(value+mLength) != '\0')
    {
        mLength++;
    }
    mLength++;

    mValue = new char[mLength];

    int i = 0;
    while( i != mLength )
    {
        mValue[i] = value[i];
        ++i;
    }
}

bool operator==(const MyString & str1, const MyString & str2) 
{
    if(!str1.Compare(str2.mValue))
        return true;
    return false;
}


bool operator<=(const MyString & str1, const MyString & str2) 
{
    if(str1.Compare(str2.mValue) <= 0)
    {
        return true;
    }
    return false;
}

bool operator>=(const MyString & str1, const MyString & str2) 
{
    if(str1.Compare(str2.mValue) >= 0)
    {
        return true;
    }
    return false;
}

bool operator>(const MyString & str1, const MyString & str2) 
{
    if(str1.Compare(str2.mValue) > 0)
    {
        return true;
    }
    return false;
}

bool operator<(const MyString & str1, const MyString & str2)
{
    if(str1.Compare(str2.mValue) < 0)
    {
        return true;
    }
    return false;
}

MyString operator+(const MyString & str1, const MyString & str2)
{
    MyString res;
    int len1 = str1.mLength;
    int len2 = str2.mLength;
    res.mLength = len1 + len2 - 1;
    if(res.mLength > MAX_ALLOCABLE_STRING)
    {
        cout << "Ponad maksymalna dlugosc napisu (100 MB)" << endl;
        res.mLength = MAX_ALLOCABLE_STRING;
        len2 = res.mLength - len1 + 1;
    }

    res.mValue = new char[res.mLength];
    for(int i = 0; i < len1; ++i)
    {
        res.mValue[i] = str1.mValue[i];
    }
    for(int i = 0; i < len2; ++i)
    {
        res.mValue[len1 + i - 1] = str2.mValue[i];
    }
    res.mValue[res.mLength - 1] = '\0';

    return res;
}

std::ostream & operator<<(std::ostream & out, const MyString & str)
{
    return out << str.mValue;
}

std::istream & operator>>(std::istream & in, MyString & str)
{
    str.~MyString();
    int mLength_max = 2;
    str.mValue = new char[mLength_max];
    char c;
    in.get(c);
    while((c != '\n') && !(in.eof()))
    {
        str.mValue[str.mLength] = c;
        str.mLength++;
        if(str.mLength == mLength_max)
        {
            mLength_max *= 2;
            char * tmp;
            tmp = (char*) realloc(str.mValue, mLength_max*sizeof(char));
            if(tmp != NULL)
                str.mValue = tmp;
            else
            {
                cout << "Blad alokacji pamieci" << endl;
                free(tmp);
            }
        }
        
        if(str.mLength == MAX_ALLOCABLE_STRING)
        {
            str.mLength--;
            in.ignore(INT_MAX, '\n');
            cout << "Przekroczono dlugosc napisu (100MB)" << endl;
            break;
        }
        in.get(c);

    }
    str.mValue[str.mLength++] = '\0';
    return in;    
}

MyString::~MyString()
{
    if(this->mValue != NULL)
    {
        mLength = 0;
        delete mValue;
    }
}

MyString::MyString(const MyString & str)
{
    this->~MyString();
    while(*(str.mValue + mLength) != '\0')
    {
        mLength++;
    }
    mLength++;

    this->mValue = new char[mLength];

    int i = 0;
    while( i != mLength )
    {
        mValue[i] = str.mValue[i];
        ++i;
    }
}

MyString & MyString::operator=(const char* str)
{
    this->~MyString();
    mLength = 0;
    while(*(str + mLength) != '\0')
    {
        mLength++;
    }
    mLength++;

    mValue = new char[mLength];

    int i = 0;
    while( i != mLength )
    {
        mValue[i] = str[i];
        ++i;
    }
    return *this;
}

MyString & MyString::operator=(const MyString & str)
{
    this->~MyString();
    mLength = 0;
    while(*(str.mValue + mLength) != '\0')
    {
        mLength++;
    }
    mLength++;

    mValue = new char[mLength];

    int i = 0;
    while( i != mLength )
    {
        mValue[i] = str.mValue[i];
        ++i;
    }
    return *this;
}

MyString & MyString::operator+=(const MyString & str)
{
    int len1 = this->mLength;
    int len2 = str.mLength;
    mLength = len1 + len2 - 1;
    if(mLength > MAX_ALLOCABLE_STRING)
    {
        cout << "Ponad maksymalna dlugosc napisu (100 MB)" << endl;
        mLength = MAX_ALLOCABLE_STRING;
        len2 = mLength - len1 + 1;
    }
    char * tmp = (char *) realloc(mValue, (mLength) * sizeof(char));

    if(tmp != NULL)
        mValue = tmp;
    else
    {
        cout << "Blad allokacji pamieci !" << endl;
    }

    for(int i = 0; i < len2; ++i)
    {
        mValue[len1 + i - 1] = str.mValue[i];
    }
    mValue[mLength - 1] = '\0';

    return *this;
}

const char* MyString::Get() const
{
    return this->mValue;
}

void MyString::Set(const char* new_value)
{
    this->~MyString();

    while(*(new_value + mLength) != '\0')
    {
        mLength++;
    }
    mLength++;

    this->mValue = new char[mLength];

    int i = 0;
    while( i != mLength )
    {
        mValue[i] = new_value[i];
        ++i;
    }
}

void MyString::Output() const
{
    cout << this->mValue << endl;
}

void MyString::Input()
{
    this->~MyString();
    int mLength_max = 2;
    mValue = new char[mLength_max];
    char c;
    cin.get(c);
    while(c != '\n')
    {
        mValue[mLength] = c;
        mLength++;
        if(mLength == mLength_max)
        {
            mLength_max *= 2;
            char * tmp;
            tmp = (char*) realloc(mValue, mLength_max*sizeof(char));
            if(tmp != NULL)
                mValue = tmp;
            else
            {
                cout << "Blad alokacji pamieci" << endl;
                free(tmp);
            }
        }

        if(mLength == MAX_ALLOCABLE_STRING)
        {
            mLength--;
            cin.ignore(INT_MAX, '\n');
            cout << "Przekroczono dlugosc napisu (100MB)" << endl;
            break;
        }
        cin.get(c);

    }
    mValue[mLength++] = '\0';
}

int MyString::Compare(const char* com_string) const
{
    int diff = 0;

    // dodatnie wartosci gdy string pozniej alfabetycznie niz w parametrze 
    // ujemne wartosci gdy string wczesniej alfabetycznie niz w parametrze
    for(int i = -1;(i+1==0) || ((diff==0) && (com_string[i] != '\0') && (this->mValue[i] != '\0')) ; ++i, diff = this->mValue[i] - com_string[i]);
    return diff;
}

unsigned int MyString::Length() const
{
    return mLength;
}
