#include <iostream>
#include <fstream>
#include <vector>

#include <stdlib.h>


using std::cin;
using std::cout;
using std::endl;

#define MAX_ALLOCABLE_STRING 104857600 // 100 MB

class MyString
{
    char * mValue = NULL;
    unsigned int mLength = 0;

    public:
        MyString(const char* value = "");
        ~MyString();
        MyString(const MyString & str);
        MyString & operator=(const char* str);
        MyString & operator=(const MyString & str);
        MyString & operator+=(const MyString & str);
        friend bool operator==(const MyString & str1, const MyString & str2);
        friend bool operator<=(const MyString & str1, const MyString & str2);
        friend bool operator>=(const MyString & str1, const MyString & str2);
        friend bool operator>(const MyString & str1, const MyString & str2);
        friend bool operator<(const MyString & str1, const MyString & str2);
        friend MyString operator+(const MyString & str1, const MyString & str2);
        friend std::ostream & operator<<(std::ostream & out, const MyString & str);
        friend std::istream & operator>>(std::istream & in, MyString & str);
        void Set(const char* value);
        const char* Get() const;
        void Output() const;
        void Input();
        int Compare(const char* com_string) const;
        unsigned int Length() const;
};
   
