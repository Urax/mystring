cmake_minimum_required(VERSION 3.16)

project(MyString)

include_directories(${CMAKE_SOURCE_DIR})



set(SOURCES 
    MyString.cpp
    main.cpp
)


add_executable(${CMAKE_PROJECT_NAME} ${SOURCES})

